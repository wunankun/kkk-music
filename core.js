function initMusic(Music, that) {
    Music.flag = true
 
    Music.timer = null

    Music.audioTime = 0
    Music.fragments = 0
    Music.autoplay = true
    Music.onEnded(() => {

        that.progressEnd()
        that.animationEnd()
        that.musicPlayEnd()
        that.setData({
            status: true
        })
        Music.stop()
    })
    Music.onPlay(() => {
        if (!Music.flag) {
            that.progressStar()
            that.animationStar()
            that.setData({
                status: false
            })
      
            Music.loadOver(that)

        }
        if (Music.flag) {
            Music.onTimeUpdate(() => {
                if (that.properties.info.flag === false) { //点击加载
                    Music.flag = false
                    that.progressStar()
                    that.animationStar()
                    Music.audioTime = parseInt(Music.duration)
                    Music.fragments = Music.duration / 200
                    that.setData({
                        status: false,
                        duration: Music.audioTime
                    })
                    Music.offTimeUpdate()
          
                    Music.loadOver(that)
                    return
                }
                Music.audioTime = parseInt(Music.duration)
                Music.fragments = Music.duration / 200
                Music.offTimeUpdate()
                Music.flag = false
                Music.pause()
                that.setData({
                    duration: Music.audioTime
                })
                Music.loadOver(that)

            })
        }
    })
    Music.onWaiting(() => {
        that.setData({
            loadFlag: true
        })
        Music.timer === null ? Music.timer = setTimeout(() => {
            wx.showToast({
                title: '音频加载失败, 请检查网络',
                icon: 'none',
                duration: 2000
            })
            Music.loadOver(that)
        }, that.properties.info.loadOverTime) : null
    })
    Music.onPause(() => {
        that.progressEnd()
        that.animationStop()
        that.setData({
            status: true
        })
    })
    Music.setUrl = (url) => {
        // Music.src = url.replace('https', 'http')
        return Music
    }
    Music.loadOver = (that) => {
        // 加载成功
        if (Music.timer !== null) {
            clearTimeout(Music.timer)
            Music.timer = null
        }
        that.setData({
            loadFlag: false
        })
        // 加载成功end
    }
}


module.exports = {
    initMusic
}