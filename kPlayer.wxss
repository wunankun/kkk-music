/* components/music/music.wxss */
.box {
    display: flex;
    align-items: center;
    padding-left: 50rpx;
    padding-bottom: 5rpx;
    box-sizing: border-box;
    position: relative;
    overflow: hidden;
    box-shadow: 0px 0px 3px #ccc;
}
.name-box {
    width: 100%;
    height: 60rpx;
    position: absolute;
    top: 0;
    left: 50%;
    transform: translateX(-50%);
    display: flex;
    align-items: center;
    padding-left: 50rpx;
    color: #fff;
    font-size: 35rpx;
    font-weight: 700;
    box-sizing: border-box;
}
.author-line {
    margin-left: 10rpx;
    margin-right: 10rpx;
}
.time-dialog {
    width: 220rpx;
    height: 80rpx;
    background-color: rgba(0,0,0, .5);
    display: flex;
    justify-content: center;
    align-items: center;
    color: #fff;
    position: fixed;
    z-index: 999;
    left: 50%;
    top: 20%;
    transform: translateX(-50%);
    border-radius: 10rpx;
}
.time-dialog-show {
    animation: time_dialog_show .2s forwards;
}
.time-dialog-hide {
    animation: time_dialog_hide .3s forwards;
}
@keyframes time_dialog_show {
    from {
        left: 50%;
        top: 22%;
        transform: translateX(-50%) scale(.5); 
        opacity: 0;
        display: flex;
    }
    to {
        left: 50%;
        top: 20%;
        transform: translateX(-50%) scale(1); 
        opacity: 1;
        display: flex;
    }
}
@keyframes time_dialog_hide {
    to {
        left: 50%;
        top: 22%;
        transform: translateX(-50%) scale(.5); 
        opacity: 0;
        display: none;
    }
}
.comminute {
    height: 28rpx;
    width: 1rpx;
    border-left: 5rpx solid #fff;
    transform: rotate(10deg);
    margin-left: 8rpx;
    margin-right: 7rpx;
}
.progress {
    height: 5rpx;
    background-color: #fff;
    position: absolute;
    bottom: 5rpx;
    border-radius: 10rpx;
}
.current-progress {
    position: relative;
    height: 5rpx;
    box-sizing: border-box;
    border: 1rpx solid #fff;
    box-shadow: 0px 0px 3px #fff;
    border-radius: 10rpx;
}
.point {
    position: absolute;
    right: -3rpx;
    top: 50%;
    transform: translateY(-50%);
    width: 12rpx;
    height: 12rpx;
    border-radius: 50%;
    border: 1px solid #fff;
    box-shadow: 0px 0px 3px #fff;
    box-sizing: border-box;
}

.status-box {
    position: relative;
    height: 50rpx;
}
.have-name .status-box {
    top:20rpx;
}
.play-box {
    position: absolute;
    left: 0;
    width: 44rpx;
    height: 50rpx;
    overflow: hidden;
}
.show-play {
    animation: play_show .3s;
}
.hide-play {
    animation: play_hide .1s forwards;
}
.play {
    position: absolute;
    width: 180rpx;
    height: 50rpx;
    border-radius: 50%;
    overflow: hidden;
    right: 0;
}
.play::after {
    content: "";
    width: 0;
    height: 0;
    border-left: 22rpx solid transparent;
    border-right: 22rpx solid transparent;
    border-bottom: 50rpx solid #fff;
    transform: rotate(90deg);
    border-radius: 10rpx;
    position: absolute;
    top: 0;
    right: -3rpx;
}
@keyframes play_show {
    0% {
        left: -10rpx;
        opacity: 0;
    }
    70% {
        left: 5rpx;
        opacity: 1;
    }
    100% {
        left: 0rpx;
        opacity: 1;

    }
}
@keyframes play_hide {
    from {
        left: 0;
        opacity: 1;
    }
    to {
        left: -10rpx;
        opacity: 0;
    }
}

.pause-box {
    position: absolute;
    left: 5rpx;
    width: 44rpx;
    height: 50rpx;
    display: flex;
    align-items: center;
    box-sizing: border-box;
}
.pause-one {
    position: absolute;
    width: 8rpx;
    height: 75%;
    background-color: #fff;
    border-radius: 4rpx;
}
.pause-two {
    position: absolute;
    left: 20rpx;
    width: 8rpx;
    height: 75%;
    background-color: #fff;
    border-radius: 4rpx;
}
.pause-show .pause-one{
    animation: pause1_show .3s;
}
.pause-show .pause-two{
    animation: pause2_show ease-in .3s;
}
.hide-pause {
    animation: pause_hide .3s forwards;
}

@keyframes pause1_show {
    from {
        left: 20rpx;
    }
    to {
        left: 0rpx;
    }
}
@keyframes pause2_show {
    0% {
        transform: scale(.8);
    }
    80% {
        transform: scale(1.1);
    }
    100% {
        transform: scale(1);
    }
}
@keyframes pause_hide {
    from {
        left: 0;
        opacity: 1;
    }
    to {
        left: 10rpx;
        opacity: 0;
    }
}
.soundbyte-box {
    height: 65%;
    width: 200rpx;
    position: absolute;
    right: 40rpx;
}
.soundbyte-box-two {
    height: 100%;
    width: 100%;
    position: relative;
}
.have-name .soundbyte-box-two {
    top:20rpx;
}
.soundbyte-box .top {
    height: 50%;
    width: 100%;
    display: flex;
    align-items: flex-end;
}
.soundbyte-box .bottom {
    height: 50%;
    width: 100%;
    display: flex;
}
.soundbyte {
    width: 5rpx;
    height: 1rpx;
    background-color: #fff;
    margin-right: 2rpx;
}

.soundbyte-animation-1 {
    animation: type_1 .3s alternate infinite;
}
.soundbyte-animation-2 {
    animation: type_2 .4s alternate infinite;
}
.soundbyte-animation-3 {
    animation: type_3 .5s alternate infinite;
}
.soundbyte-animation-4 {
    animation: type_4 .6s alternate infinite;
}
.soundbyte-animation-5 {
    animation: type_5 .7s alternate infinite;
}
.soundbyte-animation-6 {
    animation: type_6 .3s alternate infinite;
}
.soundbyte-animation-7 {
    animation: type_7 .4s alternate infinite;
}
.soundbyte-animation-8 {
    animation: type_8 .5s alternate infinite;
}
.soundbyte-animation-9 {
    animation: type_8 .6s alternate infinite;
}


@keyframes type_1 {
    from {
        height: 0rpx;
    }
    to {
        height: 5rpx;
    }
}
@keyframes type_2 {
    from {
        height: 3rpx;
    }
    to {
        height: 10rpx;
    }
}
@keyframes type_3 {
    from {
        height: 6rpx;
    }
    to {
        height: 16rpx;
    }
}
@keyframes type_4 {
    from {
        height: 10rpx;
    }
    to {
        height: 20rpx;
    }
}
@keyframes type_5 {
    from {
        height: 8rpx;
    }
    to {
        height: 25rpx;
    }
}
@keyframes type_6 {
    from {
        height: 0rpx;
    }
    to {
        height: 18rpx;
    }
}
@keyframes type_7 {
    from {
        height: 7rpx;
    }
    to {
        height: 17rpx;
    }
}
@keyframes type_8 {
    from {
        height: 13rpx;
    }
    to {
        height: 23rpx;
    }
}
@keyframes type_9 {
    from {
        height: 0rpx;
    }
    to {
        height: 25rpx;
    }
}

.soundbyte-animation-stop {
    animation-play-state:paused;
}

.loader {
    font-size: 10rpx;
    text-indent: -9999em;
    width: 50rpx;
    height: 50rpx;
    border-radius: 50%;
    background: linear-gradient(to right, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
    position: relative;
    animation: load 1.4s infinite linear;
    transform: translateZ(0);
  }
.loader:before {
    width: 50%;
    height: 50%;
    background: #ffffff;
    border-radius: 100% 0 0 0;
    position: absolute;
    top: 0;
    left: 0;
    content: '';
  }
.my-after {
    width: 75%;
    height: 75%;
    border-radius: 50%;
    margin: auto;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }
  @-webkit-keyframes load {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
  @keyframes load {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }