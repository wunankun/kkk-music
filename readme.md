# 项目说明
    kPlayer是一个微信小程序播放器组件, 引入组件后 只需要传音频url就能播放.
    含有播放动画, 音频进度条, 长按音频显示当前时间/总时间, 可以通过滑动组件来控制进度.
    通过info参数可以配置组件的外观和功能
    通过playbackStatus事件可以在组件外部监听播放状态

# 效果演示
![tutieshi_448x960_26s.gif](https://i.loli.net/2020/09/21/r1NHhjQiwbtzKJe.gif)

# 使用方法
```
// -----js----
data: {
    info: {
        //链接会失效, 请替换成自己的音频
        url: "http://qhyru96xp.hn-bkt.clouddn.com/Higher%C2%A0Brothers%26DJ%2BSnake%2B-%2BMade%2BIn%2BChina%28Special%2BEdition%29.mp3",
        width: "100%", //如果是字符串 需要带上单位, 如果是数字rpx将作为默认单位
        border: 12,
    }
}

playbackStatus(e) { //播放器的播放状态
    console.log(e.detail)
}


//-----wxml----
<kPlayer info="{{info}}" bind:playbackStatus="playbackStatus"></kPlayer>
```
# info的属性
| 属性 | 类型 | 默认值 | 必填 | 说明 |
| --- | --- | --- | --- | --- |
| url | String |  | 是 | 音频链接 |
| flag | Boolean | false | 否 | true提前加载音频, false点击后加载音频 |
| name | String |  | 否 | 音频名称, 如果带有name, 音频组件的高度自动增加50rpx用来显示name |
| author | String |  | 否 | 音频作者, 有name才会显示作者 |
| width | Number/String | '100%' | 否 | 播放器组件宽度, 最小值为360, Number类型rpx作为默认单位, 可以自己设定单位 |
| height | Number | 100 | 否 | 播放器组件高度, 单位rpx |
| color | String | #f00 | 否 | 播放器主题颜色 |
| border | Number | height / 2 | 否 | 播放器的圆角弧度, 单位rpx, 默认为圆角 |
| progressDisable | Boolean | false | 否 | true隐藏进度条 |
| longTouchDisable | Boolean | false | 否 | true禁用长按组件显示时间 |
| moveDisable | Boolean | false | 否 | true禁用组件滑动控制进度 |
| loadOverTime | Number | 30000 | 否 | 音频加载的超时时间, 单位为毫秒 |

# 事件
| 事件名 | 必填 | 说明 |
| --- | --- | --- |
| playbackStatus | 否 | 播放器的播放状态 |
# 播放器的播放状态参数
| 属性 | 类型 | 说明 |
| --- | --- | --- |
| url | String | 音频链接 |
| currentTime | Number | 当前播放时间 |
| duration | Number | 总时长 |
| status | Number | 0播放完成, 1播放, 2暂停 |
